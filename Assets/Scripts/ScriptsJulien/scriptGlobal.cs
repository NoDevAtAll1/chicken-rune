﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scriptGlobal : MonoBehaviour
{
	//Etat du jeu
	public static string etatJeu = "init";
    public static bool sacrificeFinie = false;
	public static bool joueur2Mode = false;

	//Nombre de vagues
	public static int pallierVagues = 10;
	public static int nbrVagues = 1;

	//Score
	public static int score = 0;
	public static int scoreMax = 0;
	public static int ptsEnnemi = 10;
	public static int ptsBoss = 100;

	//Boos Time
	public static bool bossTime = false;

	//Stage
	public static int stageActu = 1;

	//Vie du poulet
	public static int viePouletMax = 3;
	public static int viePoulet = 3;
	public static float vitessePoulet = 3;

	//Etat du poulet
	public static bool pouletInvincible = false;

	//Liste des ennemis
	public static List<Cultiste> listeCultistes;

	public const float tempsVague1 = 6; 
	public const float tempsVague2 = 12;
	public static float tempsEntreVagues = tempsVague1;
	public static float interval2ennemis = 0.6f;

	//Les bonus
	//Probabilite d'apparition d'un bonus
	public static int pourcBonus = 20;

	//Boost vitesse
	public static float dureeBoostVitesse = 3;
	public static float gainBoostVitesse = 3;
	public static float tempsDeVieBoostVitesse = 8;
	//Level up
	public static float tempsDeVieLevelUp = 8;
	//Slow ennemi
	public static float dureeSlowEnnemi = 3;
	public static float modifSlowEnnemi = 0;
	public static float tempsDeVieSlowEnnemi = 7;
	//Slow ennemi
	public static float tempsDeVieCoeur = 10;

    //Rune
    public static bool sortEnCour = false;

	//Audio
	public static float volumeMusique = 0.8f;
}
