﻿using UnityEngine;
using System.Collections;

public class scriptFX : MonoBehaviour
{		
	//Variables
	public static scriptFX Instance;

	public AudioClip FxRune1;
	public AudioClip FxRune2;
	public AudioClip FxRune3;
	public AudioClip FxRune4;
	public AudioClip FxRune5;
	public AudioClip FxPowerUp;
	public AudioClip FxCultiste1;
	public AudioClip FxHit1;
	public AudioClip FxHit2;
	public AudioClip FxMort1;
	public AudioClip FxFireBall1;
	public AudioClip FxBossApparition;
	public AudioClip FxBossMort1;
	public AudioClip FxCultistMort1;
	public AudioClip FxTransitionStage;
	public AudioClip FxSwitch1;
	public AudioClip FxTransitionMort;
	public AudioClip FxLevelComplete;
	public AudioClip FxClic1;
	public AudioClip FxIntro1;
	public AudioClip FxIntro2;
	public AudioClip FxIntro3;
	public AudioClip FxIntro4;

	void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("Multiple instances of SoundEffectsHelper!");
		}
		Instance = this;
	}

	public void MakeFx(string idAudio)
	{
		switch(idAudio)
		{
		case "FxRune1" :
			MakeSound(FxRune1);
				break;	
		case "FxRune2" :
			MakeSound(FxRune2);
				break;
		case "FxRune3" :
			MakeSound(FxRune3);
				break;
		case "FxRune4" :
			MakeSound(FxRune4);
				break;
		case "FxRune5" :
			MakeSound(FxRune5);
				break;
		case "FxPowerUp" :
			MakeSound(FxPowerUp);
			break;
		case "FxCultiste1" :
			MakeSound(FxCultiste1);
			break;
		case "FxHit1" :
			MakeSound(FxHit1);
			break;
		case "FxHit2" :
			MakeSound(FxHit2);
			break;
		case "FxMort1" :
			MakeSound(FxMort1);
			break;
		case "FxFireBall1" :
			MakeSound(FxFireBall1);
			break;
		case "FxBossMort1" :
			MakeSound(FxBossMort1);
			break;
		case "FxBossApparition" :
			MakeSound(FxBossApparition);
			break;
		case "FxCultistMort1" :
			MakeSound(FxCultistMort1);
			break;
		case "FxTransitionStage" :
			MakeSound(FxTransitionStage);
			break;
		case "FxSwitch1" :
			MakeSound(FxSwitch1);
			break;
		case "FxTransitionMort" :
			MakeSound(FxTransitionMort);
			break;
		case "FxLevelComplete" :
			MakeSound(FxLevelComplete);
			break;
		case "FxClic1" :
			MakeSound(FxClic1);
			break;
		case "FxIntro1" :
			MakeSound(FxIntro1);
			break;
		case "FxIntro2" :
			MakeSound(FxIntro2);
			break;
		case "FxIntro3" :
			MakeSound(FxIntro3);
			break;
		case "FxIntro4" :
			MakeSound(FxIntro4);
			break;
		}
	}

	//Lance la lecture d'un son
	private void MakeSound(AudioClip originalClip)
	{
		this.GetComponent<AudioSource>().PlayOneShot(originalClip);
	}
}
