﻿using UnityEngine;
using System.Collections;

public class scriptMusiques : MonoBehaviour
{
	//Variables
	public static scriptMusiques Instance;
	public AudioClip ThemePrincipal;
	public AudioClip ThemeStage1;
	public AudioClip ThemeStage2;
	public AudioClip ThemeStage3;
	public AudioClip ThemeStage4;
	public AudioClip ThemeStage5;
	public AudioClip ThemeBoss1;
	public AudioClip ThemeBoss2;

	private float saveVolume;

	void Awake()
	{
		if(Instance != null)
		{
			Debug.LogError("Multiple instances of SoundEffectsHelper!");
		}
		Instance = this;

		saveVolume = scriptGlobal.volumeMusique;
	}

	void Update()
	{
		if(scriptGlobal.volumeMusique != saveVolume)
		{
			saveVolume = scriptGlobal.volumeMusique;
			GetComponent<AudioSource>().volume = scriptGlobal.volumeMusique;
		}
	}

	public void MakeTheme(string idMusique)
	{
		switch(idMusique)
		{
		case "ThemePrincipal" :
			GetComponent<AudioSource>().clip = ThemePrincipal;
			break;
		case "ThemeStage1" :
			GetComponent<AudioSource>().clip = ThemeStage1;
			break;
		case "ThemeStage2" :
			GetComponent<AudioSource>().clip = ThemeStage2;
			break;
		case "ThemeStage3" :
			GetComponent<AudioSource>().clip = ThemeStage3;
			break;
		case "ThemeStage4" :
			GetComponent<AudioSource>().clip = ThemeStage4;
			break;
		case "ThemeStage5" :
			GetComponent<AudioSource>().clip = ThemeStage5;
			break;
		case "ThemeBoss1" :
			GetComponent<AudioSource>().clip = ThemeBoss1;
			break;
		case "ThemeBoss2" :
			GetComponent<AudioSource>().clip = ThemeBoss2;
			break;
		}
		GetComponent<AudioSource>().volume = scriptGlobal.volumeMusique;
		GetComponent<AudioSource>().Play();
		GetComponent<AudioSource>().loop = true;
	}

	public void StopTheme()
	{
		GetComponent<AudioSource>().Stop();
	}

	public void FadeTheme()
	{
		StartCoroutine(BaisserVolume());
	}
	IEnumerator BaisserVolume()
	{
		yield return new WaitForSeconds(0.1f);
		GetComponent<AudioSource>().volume -= 0.20f;
		yield return new WaitForSeconds(0.1f);
		GetComponent<AudioSource>().volume -= 0.20f;
		yield return new WaitForSeconds(0.1f);
		GetComponent<AudioSource>().volume -= 0.20f;
		yield return new WaitForSeconds(0.1f);
		GetComponent<AudioSource>().volume -= 0.20f;
		yield return new WaitForSeconds(0.1f);
		GetComponent<AudioSource>().volume -= 0.20f;
		GetComponent<AudioSource>().Stop();
		GetComponent<AudioSource> ().volume = 1;
	}
}