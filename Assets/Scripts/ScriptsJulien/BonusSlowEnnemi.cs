﻿using UnityEngine;
using System.Collections;

public class BonusSlowEnnemi : MonoBehaviour {

	// Use this for initialization
	void Start()
	{
		StartCoroutine (GestionVie());
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.name.Equals ("Joueur")|| other.gameObject.name.Equals("Joueur2") && scriptGlobal.etatJeu == "play") {
			//Level up du poulet
			GameObject.Find ("Scripts").GetComponent<Main> ().SlowEnnemi ();
			//On detruit le level up
			Destroy (this.gameObject);
		}
	}

	IEnumerator GestionVie() {
		yield return new WaitForSeconds(scriptGlobal.tempsDeVieSlowEnnemi-2);
		//On fait clignoter le bonus
		gameObject.transform.GetComponent<SpriteRenderer>().color = new Color (gameObject.transform.GetComponent<SpriteRenderer>().color.r, gameObject.transform.GetComponent<SpriteRenderer>().color.g, gameObject.transform.GetComponent<SpriteRenderer>().color.b, 0.1f);
		yield return new WaitForSeconds(0.5f);
		gameObject.transform.GetComponent<SpriteRenderer>().color = new Color (gameObject.transform.GetComponent<SpriteRenderer>().color.r, gameObject.transform.GetComponent<SpriteRenderer>().color.g, gameObject.transform.GetComponent<SpriteRenderer>().color.b, 1f);
		yield return new WaitForSeconds(0.5f);
		gameObject.transform.GetComponent<SpriteRenderer>().color = new Color (gameObject.transform.GetComponent<SpriteRenderer>().color.r, gameObject.transform.GetComponent<SpriteRenderer>().color.g, gameObject.transform.GetComponent<SpriteRenderer>().color.b, 0.1f);
		yield return new WaitForSeconds(0.5f);
		gameObject.transform.GetComponent<SpriteRenderer>().color = new Color (gameObject.transform.GetComponent<SpriteRenderer>().color.r, gameObject.transform.GetComponent<SpriteRenderer>().color.g, gameObject.transform.GetComponent<SpriteRenderer>().color.b, 1f);
		yield return new WaitForSeconds(0.5f);
		//On detruit le level up
		Destroy(this.gameObject);
	}

	public void OnDestroy() {
		StopCoroutine (GestionVie());
	}
}
