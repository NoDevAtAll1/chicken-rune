﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MiseEnSceneSacrifice : MonoBehaviour {
    public void lancerSacrifice()
    {
        scriptGlobal.etatJeu = "sacrifice";
		//Musique
		//scriptFX.Instance.MakeFx("FxIntro3");
    }
    public void finSacrifice()
    {
		scriptGlobal.sacrificeFinie = true;
        scriptGlobal.etatJeu = "play";
        //Supprimmer les GameObject de l'intro :
        Destroy(GameObject.Find("Introduction"));
        //Relancer Jeu :
        GameObject.Find("Scripts").GetComponent<Main>().message.GetComponent<Text>().text = "SAVE YOUR LIFE NEW CHICKEN !!!";
		//On vire les bandes noires de cinematiques
		iTween.MoveTo(GameObject.Find("Canvas").transform.FindChild("Cine1").gameObject, iTween.Hash("position", new Vector3(GameObject.Find("Canvas").transform.FindChild("Cine1").gameObject.transform.position.x, GameObject.Find("Canvas").transform.FindChild("Cine1").gameObject.transform.position.y + Screen.height / 4, 0f), "time", 1f, "oncomplete", "", "easetype", "Linear", "oncompletetarget", gameObject));
		iTween.MoveTo(GameObject.Find("Canvas").transform.FindChild("Cine2").gameObject, iTween.Hash("position", new Vector3(GameObject.Find("Canvas").transform.FindChild("Cine2").gameObject.transform.position.x, GameObject.Find("Canvas").transform.FindChild("Cine2").gameObject.transform.position.y - Screen.height / 4, 0f), "time", 1f, "oncomplete", "", "easetype", "Linear", "oncompletetarget", gameObject));
        //On affiche les infos combi
		GameObject.Find("Canvas").transform.FindChild("InfoCombi").gameObject.SetActive(true);
		GameObject.Find("Scripts").GetComponent<Main>().ResetJeu();
    }
}
