﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {
    
	public float tempsAvantPremierProjectile = 30;
    public float tempsEntreProjectile = 30;

	//Use this for initialization
	void Start ()
	{
        //InvokeRepeating("instancierProjectil", tempsAvantPremierProjectile, tempsEntreProjectile);
    }

	public void initProjectil()
	{
		InvokeRepeating("instancierProjectil", tempsAvantPremierProjectile, tempsEntreProjectile);
	}

	private void instancierProjectil()
    {
        if (!scriptGlobal.etatJeu.Equals("dead"))
        {
            //Fx
			scriptFX.Instance.MakeFx("FxFireBall1");
			GameObject unProjectile = Instantiate(Resources.Load("bouleFeu")) as GameObject;
            unProjectile.transform.position = this.gameObject.transform.position;
        }
        
    }


}
