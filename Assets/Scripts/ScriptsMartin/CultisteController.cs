﻿using UnityEngine;
using System.Collections;

public class CultisteController : MonoBehaviour
{
    private bool _flipped = false;
    private GameObject Cible;
    private GameObject Cible2;
    private GameObject CibleActuel;
    private bool contactAvecJoueur=false;
    private bool JoueurToucher = false;
    public int Type = 0;
    public int Vie = 1;
    public float Vitesse = 1;
	public int vague = 1;
    private bool mort = false;
    private string derniereJoueurContact;

    void Awake()
    {
        Cible = GameObject.Find("Joueur");
		Cible2 = GameObject.Find("Scripts").GetComponent<Main>().poulet2;
        CibleActuel = Cible;
        /*if(scriptGlobal.joueur2Mode)
        {
            Cible2 = GameObject.Find("Joueur2");
        }*/
	}

    void FixedUpdate()
    {
        if(scriptGlobal.joueur2Mode)
        {
            Vector3[] tabPosition = new Vector3[2] { (Cible.transform.position), (Cible2.transform.position) };
            if((tabPosition[0].x-this.transform.position.x)+(tabPosition[0].y - this.transform.position.y) < (tabPosition[1].x - this.transform.position.x) + (tabPosition[1].y - this.transform.position.y))
            {
                CibleActuel = Cible;
            }
            else
            {
                CibleActuel = Cible2;
            }
        }
        else
        {
            CibleActuel = Cible;
        }
		if(contactAvecJoueur == false && scriptGlobal.etatJeu == "play" && mort == false)
        {
			if(!GetComponent<Animator> ().GetCurrentAnimatorStateInfo(0).IsName("Respawn")) transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, CibleActuel.transform.position, (0.01f*(Vitesse+scriptGlobal.modifSlowEnnemi)));
            float dir = CibleActuel.transform.position.x - this.GetComponent<Transform>().position.x;
            GetComponent<Animator>().SetFloat("direction",dir);
            if (dir > 0 && !this._flipped) this.Flip();
            if (dir < 0 && this._flipped) this.Flip();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        
		if((other.gameObject.name.Equals("Joueur")|| other.gameObject.name.Equals("Joueur2")) && contactAvecJoueur == false && JoueurToucher == false && mort == false && scriptGlobal.etatJeu == "play")
        {
            derniereJoueurContact = other.gameObject.name;
            //print("Toucher Couller");
            contactAvecJoueur = true;
            JoueurToucher = true;
			//Perte de vie du poulet
			GameObject.Find("Scripts").GetComponent<Main>().ModifierVie(-1);
			this.GetComponent<Animator> ().SetTrigger ("attack");
			//Debug.Log ("set trigger hurt");
			other.GetComponent<Animator>().SetTrigger("hurt");
            //Lancer Chrono joueur intouchable :
            Invoke("stopInvincibiliter", 2);
            //Destroy(other.gameObject);
            //Lancer Anim ?
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        
        if (other.gameObject.name.Equals(derniereJoueurContact) &&contactAvecJoueur == true)
        {
            //print("Louper Couller");
            contactAvecJoueur = false;
            //Lancer anim ?
        }
    }
    private void stopInvincibiliter()
    {
        JoueurToucher = false;
        //print("Invincibiliter OFF");
    }
    public void infligerDegat(int nbrDegats)
    {
		//on verifie si c'est un boss
		bool isBoss = false;
		if(gameObject.name.Substring (0, 8) == "CultBoss")
		{
			isBoss = true;
		}

        //print("Rentrer dans infliger dégats"); 
        if (Vie - nbrDegats <= 0)
        {
			//Invincibilite on
			if(isBoss == true) scriptGlobal.pouletInvincible = true;
			//Lancer Animation de mort :
			GetComponent<Animator>().SetTrigger("dead");
            //Generation du bonus :
			GameObject.Find("Scripts").GetComponent<Main>().GestionApparitionBonus(this.gameObject.transform.position, isBoss);
            //Suppression du Cultiste :
            GameObject.Find("pentagramme").GetComponent<pentagrammeScript>().ennemiASupprimer.Add(this.gameObject);
			if(isBoss)
			{
				GameObject.Find("Scripts").GetComponent<Main>().nextLevelBefore();
				//Ajout du score
				GameObject.Find("Scripts").GetComponent<Main>().MaJscore(scriptGlobal.ptsBoss*scriptGlobal.stageActu);
			}
			else
			{
				//Ajout du score
				//On controle la vague durant laquelle il a ete creee
				//Si ce n'est pas la meme, le score est diminue
				if(vague == scriptGlobal.nbrVagues) GameObject.Find("Scripts").GetComponent<Main>().MaJscore(scriptGlobal.ptsEnnemi*scriptGlobal.stageActu);
				else GameObject.Find("Scripts").GetComponent<Main>().MaJscore(scriptGlobal.ptsEnnemi*scriptGlobal.stageActu/(1+(scriptGlobal.nbrVagues-vague)));
			}
            mort = true;
            Invoke("destroyThisCultiste", 0.80f);
        }
        else
        {
            Vie -= nbrDegats;
			//Fx
			scriptFX.Instance.MakeFx("FxHit2");
			//Anim
			if(isBoss) GetComponent<Animator>().SetTrigger("hurt");
        }
    }

    // Flip
    void Flip()
    {
        Vector3 scale = GetComponent<Transform>().localScale;
        scale.x *= -1;
        GetComponent<Transform>().localScale = scale;
        if (this._flipped) this._flipped = false;
        else this._flipped = true;
    }
    public void destroyThisCultiste()
    {
        Destroy(this.gameObject);
    }
}
