﻿using UnityEngine;
using System.Collections;

public class ControllerCultisteSacrifice : MonoBehaviour {
    private bool _flipped = false;
    private GameObject Cible;
    private bool contactAvecJoueur = false;
    private bool JoueurToucher = false;
    public int Type = 0;
    public int Vie = 1;
    public float Vitesse = 1;
    private bool mort = false;
    
	// Use this for initialization
    void Start () {
        Cible = GameObject.Find("PouletSacrifier");
    }
    
	void FixedUpdate()
    {
        if (contactAvecJoueur == false && scriptGlobal.etatJeu == "sacrifice" && mort == false)
        {
            if (!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Respawn")) transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, Cible.transform.position, (0.01f * Vitesse));
            float dir = Cible.transform.position.x - this.GetComponent<Transform>().position.x;
            GetComponent<Animator>().SetFloat("direction", dir);
            if (dir > 0 && !this._flipped) this.Flip();
            if (dir < 0 && this._flipped) this.Flip();
        }
    }
    
	void OnTriggerEnter2D(Collider2D other)
    {

        if(other.gameObject.name.Equals("PouletSacrifier") && contactAvecJoueur == false && JoueurToucher == false && mort == false)
        {
            contactAvecJoueur = true;
            this.GetComponent<Animator>().SetTrigger("attack");
            //Debug.Log ("set trigger hurt");
            other.GetComponent<Animator>().SetTrigger("hurt");
			scriptFX.Instance.MakeFx("FxHit1");
            other.GetComponent<ScriptPouletSacrifice>().prendreDegat();
        }
    }
   
	private void stopInvincibiliter()
    {
        JoueurToucher = false;
        //print("Invincibiliter OFF");
    }
    
	public void infligerDegat(int nbrDegats)
    {
        //print("Rentrer dans infliger dégats"); 
        if (Vie - nbrDegats <= 0)
        {
            //Lancer Animation de mort :
            GetComponent<Animator>().SetTrigger("dead");
            //on verifie si c'est un boss
            bool isBoss = false;
            if (gameObject.name.Substring(0, 8) == "CultBoss")
            {
                isBoss = true;
                this.gameObject.name = "OldDeadBoss";
            }
            //Suppression du Cultiste :
            GameObject.Find("pentagramme").GetComponent<pentagrammeScript>().ennemiASupprimer.Add(this.gameObject);
            if(isBoss) GameObject.Find("Scripts").GetComponent<Main>().nextLevelBefore();
            mort = true;
            Invoke("destroyThisCultiste", 0.80f);
        }
        else
        {
            Vie -= nbrDegats;
        }
    }

    // Flip
    void Flip()
    {
        Vector3 scale = GetComponent<Transform>().localScale;
        scale.x *= -1;
        GetComponent<Transform>().localScale = scale;
        if (this._flipped) this._flipped = false;
        else this._flipped = true;
    }
    
	public void destroyThisCultiste()
    {
        Destroy(this.gameObject);
    }
}
