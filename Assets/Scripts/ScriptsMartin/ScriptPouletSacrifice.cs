﻿using UnityEngine;
using System.Collections;

public class ScriptPouletSacrifice : MonoBehaviour {
    public int vie = 2;
    
	public void prendreDegat()
    {
        vie -= 1;
		if(vie <= 0 && scriptGlobal.etatJeu != "dead")
        {
            scriptGlobal.etatJeu = "dead";
			//Mort du poulet
			scriptFX.Instance.MakeFx("FxIntro4");
            this.GetComponent<Animator>().SetTrigger("dead");
            Invoke("mortDuPoulet", 2.5f);
        }
    }
    
	private void mortDuPoulet()
    {
		this.gameObject.SetActive(false);
        GameObject.Find("World").GetComponent<MiseEnSceneSacrifice>().finSacrifice();
    }
}
