﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RunesManager : MonoBehaviour
{
    private List<Sort> listSorts = new List<Sort>();
    public List<int> runeActiver = new List<int>();
    public GameObject monPentaStyle;
    private List<int> listTempEnnemiCibe;
    public GameObject Pentagramme;
    
	void Awake()
    {
        //Sort 1
        List<int> combinaisonSort1 = new List<int>();
        combinaisonSort1.Add(0);
        combinaisonSort1.Add(4);
        List<int> ennemiCibleSort1 = new List<int>();
        ennemiCibleSort1.Add(0);
        listSorts.Add(new Sort(combinaisonSort1, ennemiCibleSort1));
        //Sort 2
        List<int> combinaisonSort2 = new List<int>();
        combinaisonSort2.Add(1);
        combinaisonSort2.Add(3);
        List<int> ennemiCibleSort2 = new List<int>();
        ennemiCibleSort2.Add(1);
        listSorts.Add(new Sort(combinaisonSort2, ennemiCibleSort2));
        //Sort 3
        List<int> combinaisonSort3 = new List<int>();
        combinaisonSort3.Add(3);
        combinaisonSort3.Add(2);
        combinaisonSort3.Add(4);
        List<int> ennemiCibleSort3 = new List<int>();
        ennemiCibleSort3.Add(2);
        listSorts.Add(new Sort(combinaisonSort3, ennemiCibleSort3));
        //Sort 4
        List<int> combinaisonSort4 = new List<int>();
        combinaisonSort4.Add(4);
        combinaisonSort4.Add(2);
        combinaisonSort4.Add(0);
        List<int> ennemiCibleSort4 = new List<int>();
        ennemiCibleSort4.Add(3);
        listSorts.Add(new Sort(combinaisonSort4, ennemiCibleSort4));
        //Sort 6 BOSS
        List<int> combinaisonSort6 = new List<int>();
        combinaisonSort6.Add(0);
        combinaisonSort6.Add(1);
        combinaisonSort6.Add(3);
        List<int> ennemiCibleSort6 = new List<int>();
        ennemiCibleSort6.Add(6);
        listSorts.Add(new Sort(combinaisonSort6, ennemiCibleSort6));
    }
    
	public void addRune(int maRune)
    {
        runeActiver.Add(maRune);
        testerSort();
    }
    
	private void testerSort()
    {
        bool sortLancer = false;
        foreach (Sort unSort in listSorts)
        {
            if (unSort.combinaison.Count == runeActiver.Count)
            {
                //print("Rentrer 1 testerSort");
                bool correspondance = true;
                for (int i = 0; i < runeActiver.Count; i++)
                {
                    if (runeActiver[i] != unSort.combinaison[i])
                    {
                        //print("Pas la bonne combinaison");
                        correspondance = false;
                    }
                }
                if (correspondance == true)
                {
                    effacerPentaStyle();
                    scriptGlobal.sortEnCour = true;
                    sortLancer = true;
                    sortReussi(unSort.ennemiCible);
                    //lancer animation du sort :
                    //print("listSorts.IndexOf(unSort) : " + listSorts.IndexOf(unSort));
                    GameObject.Find("Scripts").GetComponent<Main>().FxPentaBrille(3, listSorts.IndexOf(unSort));
                }
            }
        }
        if (!sortLancer)
        {
            montrerPossibiliterSuivante();
        }
    }
    
	private void sortReussi(List<int> typeEnnemiCible)
    {
        //Sort Finie :
        listTempEnnemiCibe = typeEnnemiCible;
        Invoke("sortFini", 3);//Ou lancer par l'anim a voir si possible

    }
    
	public void sortFini()
    {
        scriptGlobal.sortEnCour = false;
        if (scriptGlobal.etatJeu == "play")
        {
            //Supprimmer les ennemi dans le trigger du pentagramme qui correspond aux type :
            Pentagramme.GetComponent<pentagrammeScript>().killEnnemi(listTempEnnemiCibe);
            //Desactiver runes :
            foreach (Transform uneRune in Pentagramme.transform)
            {
                uneRune.GetComponent<RuneScript>().desactiverRune();
            }
        }
    }

    public void resetRunes()
    {
        foreach (Transform uneRune in Pentagramme.transform)
        {
            uneRune.GetComponent<RuneScript>().desactiverRune();
        }
    }

    public void effacerPentaStyle()
    {
        //On désactive les liens :
        foreach (Transform child in monPentaStyle.transform)
        {
            child.gameObject.SetActive(false);
        }
    }
    
	public void montrerPossibiliterSuivante()
    {
        effacerPentaStyle();
        if (runeActiver.Count != 0)
        {
            foreach (Sort unSort in listSorts)
            {
                bool verification = true;
                for (int i = 0; i < runeActiver.Count; i++)
                {
                    if (unSort.combinaison.Count > i)
                    {
                        if (unSort.combinaison[i] != runeActiver[i])
                        {
                            verification = false;
                        }
                    }
                }
                if (verification)
                {
                    int derniereRuneActiver = unSort.combinaison[runeActiver.Count-1];
                    if(runeActiver.Count< unSort.combinaison.Count)
                    {
                        string nomGameObject= "";
                        switch (derniereRuneActiver)
                        {
                            case 0:
                                nomGameObject = "R";
                                break;
                            case 1:
                                nomGameObject = "B";
                                break;
                            case 2:
                                nomGameObject = "P";
                                break;
                            case 3:
                                nomGameObject = "G";
                                break;
                            case 4:
                                nomGameObject = "Y";
                                break;
                        }
                        nomGameObject += "to";
                        switch (unSort.combinaison[runeActiver.Count])
                        {
                            case 0:
                                nomGameObject += "R";
                                break;
                            case 1:
                                nomGameObject += "B";
                                break;
                            case 2:
                                nomGameObject += "P";
                                break;
                            case 3:
                                nomGameObject += "G";
                                break;
                            case 4:
                                nomGameObject += "Y";
                                break;
                        }
                        monPentaStyle.transform.FindChild(nomGameObject).gameObject.SetActive(true);

                    }
                }
            }
        }
    }
}
