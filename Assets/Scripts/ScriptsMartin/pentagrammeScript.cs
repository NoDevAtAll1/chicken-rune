﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class pentagrammeScript : MonoBehaviour {
    
	public List<GameObject> ennemiPresentDansLePentagramme= new List<GameObject>();
    public List<GameObject> ennemiASupprimer = new List<GameObject>();

    void OnTriggerEnter2D(Collider2D other)
    {
        bool trouver = false;
        foreach(GameObject unGameObjectPresentDansLaList in ennemiPresentDansLePentagramme)
        {
            if(unGameObjectPresentDansLaList== other.gameObject)
            {
                trouver = true;
            }
        }
        if (trouver == false)
        {
            ennemiPresentDansLePentagramme.Add(other.gameObject);
        }
    }
    
	void OnTriggerExit2D(Collider2D other)
    {
        ennemiPresentDansLePentagramme.Remove(other.gameObject);
    }
    
	public void killEnnemi(List<int> type)
    {
        //Fx tremblement
		if(type != null)
		{
			if(type.Count > 0)
			{
				GameObject.Find("Scripts").GetComponent<Main>().cTremblementCamera = 0;
				StartCoroutine(GameObject.Find("Scripts").GetComponent<Main>().TremblementCameraH());
			}
		}
		List<string> mesColliderCheck = new List<string>();
        ennemiASupprimer.Clear();
        //print("rentrer dans kill ennemi");
        //print("Nombre objet : " + ennemiPresentDansLePentagramme.Count);
        foreach(GameObject unObjet in ennemiPresentDansLePentagramme)
        {
            string nomObjet = unObjet.name.Substring(0, 4);
            //print(nomObjet);
            if(nomObjet.Equals("Cult"))
            { 
                //print("L'objet est bien un Cult");
                foreach (int unType in type)
                {
                    //print("unType =" + unType.ToString());
                    //print("Le type de CultisteController est = " + unObjet.GetComponent<CultisteController>().Type.ToString());
                    if(unObjet.GetComponent<CultisteController>().Type == unType)
                    {
                            mesColliderCheck.Add(unObjet.name);
                            //print("Renter dans le type est le bon type !");
                            unObjet.GetComponent<CultisteController>().infligerDegat(1);
                    }
                }
            }
        }
        for(int i = ennemiASupprimer.Count - 1; i >= 0; i--)
        {
            ennemiPresentDansLePentagramme.Remove(ennemiASupprimer[i]);
        }
    }
}
