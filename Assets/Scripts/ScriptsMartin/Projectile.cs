﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	//Variables
    public Vector3 positionCible;
    private bool JoueurToucher = false;
    private string nomJoueur;
	public float speed = 0.5F;
	private float startTime;
	private float journeyLength;
    
	//Use this for initialization
    void Start()
	{
        positionCible = GameObject.Find("Joueur").transform.position;
        if (scriptGlobal.joueur2Mode)
        {
            GameObject Cible = GameObject.Find("Joueur");
            GameObject Cible2 = GameObject.Find("Joueur2");
            Vector3[] tabPosition = new Vector3[2] { (Cible.transform.position), (Cible2.transform.position) };
            if ((tabPosition[0].x - this.transform.position.x) + (tabPosition[0].y - this.transform.position.y) < (tabPosition[1].x - this.transform.position.x) + (tabPosition[1].y - this.transform.position.y))
            {
                positionCible = Cible.transform.position;
            }
            else
            {
                positionCible = Cible2.transform.position;
            }
        }

		startTime = Time.time;
		journeyLength = Vector3.Distance(this.GetComponent<Transform>().position, positionCible);

        Invoke("supprimerProjectile", 1.5f);
    }
    
	void FixedUpdate()
    {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, positionCible, fracJourney);
    }
    
	void OnTriggerEnter2D(Collider2D other)
    {
		if((other.gameObject.name.Equals("Joueur")|| other.gameObject.name.Equals("Joueur2")) && JoueurToucher == false && scriptGlobal.pouletInvincible == false)
        {
            nomJoueur = other.gameObject.name;
            JoueurToucher = true;
            //Perte de vie du poulet
            GameObject.Find("Scripts").GetComponent<Main>().ModifierVie(-1);
			other.GetComponent<Animator>().SetTrigger("hurt");
        }
    }
    
	void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.name.Equals(nomJoueur) && JoueurToucher == true)
        {
            JoueurToucher = false;
        }
    }
    
	public void supprimerProjectile()
    {
        Destroy(this.gameObject);
    }

}
